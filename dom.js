const myButton = document.querySelector('button');
myButton.addEventListener('click', showAnagram);

// função para mostrar informações na tag aside
function showAside() {
  let myParagraph = document.createElement('p');
  myParagraph.innerHTML = `Foram encontradas ${myExpressions.length} palavras que possuem 5 ou mais anagramas. Abaixo, a relação de todas as expressões regulares encontradas que foram utilizadas para gerar o resultado: `;
  asideBox.appendChild(myParagraph)

  for (let i = 0; i < resultAnagram.length; i++) {
    let myExpRegParagraph = document.createElement('p');
    myExpRegParagraph.className = 'outputExp';
    myExpRegParagraph.innerHTML = `${i} => ${myExpressions[i]}`;
    asideBox.appendChild(myExpRegParagraph);
  }
}
showAside();


// função showAnagram() mostra resultado no browser após o click
function showAnagram() {
  getSetsOfFiveAnagrams(finalArray);    //executa a função que retorna o objeto anagramSet com os resultados

  for (let value in anagramSet) {
    let myElement = document.createElement('div');
    myElement.innerHTML = `${value} => ${anagramSet[value]}`;
    outputAnagrams.appendChild(myElement);
  }
};

