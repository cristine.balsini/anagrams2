const newArray = [];          //armazena palavras ordenadas alfabeticamente
const finalArray = [];        //cópia do newArray, usado ao chamar a função getSetsOfFiveAnagrams no dom. Não é modificado pelo sort().
const resultAnagram = [];     //armazena as palavras que possuem 5 ou mais correspondências (estão ordenadas alfabeticamente)
const myWords = [];           //armazena o resultAnagram tratado (sem pontos)
const transfPalavras = [];    //armazena o array palavras tratado (sem pontos)
const myExpressions = [];     //expressoes regulares como array
const anagramSet = {}         //objeto que contém o resultado de palavras com 5 ou mais anagramas


//função que retorna uma string com as letras em ordem alfabética
function alphabetize(x) {
  return x.toLowerCase().split('').sort().join('').trim();
}


//função que aplica a função alphabetize em todo array de palavras e retornar um novo array
function isArray(myArray, output) {
  for (let i = 0; i < myArray.length; i++) {
    output.push(alphabetize(myArray[i]));
  }
  return output;
}
isArray(palavras, newArray);
isArray(palavras, finalArray)


//função que encontra as palavras que possuem 5 ou mais correspondências
function findAnagram(myArray) {

  let sortedArray = myArray.sort();
  let result = [];

  for (let i = 0; i < sortedArray.length; i++) {
    if (sortedArray[i] === sortedArray[i + 3]) {   //encontra palavras que tem 3 ou mais matches (pois preciso ter no mínimo 2 iguais para match na proxima iteraçao)
      result.push(sortedArray[i])
    }
  }

  for (let i = 0; i < result.length; i++) {
    if (result[i] === result[i + 1] & result[i + 1] !== result[i + 2]) {
      resultAnagram.push(result[i])
    }
  }

  return resultAnagram;   //apenas palavras com mais de 5 matches e que servirão para busca no array palavras
}
findAnagram(newArray)


// função que retira todos os '.' (pontos) das strings para tratar as validações
function replaceDot(myArray, output) {
  for (let i = 0; i < myArray.length; i++) {
    output.push(myArray[i].replace('.', ''))
  }
  return output;
}
replaceDot(resultAnagram, myWords);
replaceDot(palavras, transfPalavras)


//função que transforma as palavras com 5 ou mais correspondências em expressões regulares
function myRegEx(myArray) {
  for (let i = 0; i < myArray.length; i++) {
    myExpressions.push(new RegExp(myArray[i], 'g'));
  }
  return myExpressions;
}
myRegEx(myWords);


//função que encontra os anagramas (executada no dom)
function getSetsOfFiveAnagrams(myArray1) {

  for (let z = 0; z < myExpressions.length; z++) {
    let myString = myWords[z];
    anagramSet[myString] = [];

    for (let i = 0; i < myArray1.length; i++) {
      let y = (myArray1[i].match(myString));

      if (y !== null) {
        if (myString.length === transfPalavras[i].length) {
          anagramSet[myString].push(`${transfPalavras[i]}`);        //armazena no objeto com a propriedade correspondente 
        }
      }
    }
  }
  return anagramSet;
}
